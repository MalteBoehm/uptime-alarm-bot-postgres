FROM openjdk:15-oracle

MAINTAINER Malte Böhm <boemalte@gmail.com>

ADD backend/target/discord-bot-0.jar app.jar

EXPOSE 5000

CMD ["sh" , "-c", "java -jar -Dserver.port=5000 -Dspring.data.mongodb.uri=$MONGO_DB_URI -Dtoken=$TOKEN  app.jar"]
