package com.discord.bot.service;

import com.discord.bot.discord.DiscordBot;
import com.discord.bot.discord.HelperToMarkDown;
import com.discord.bot.enums.Commands;
import com.discord.bot.model.PingAndDowntime;
import com.discord.bot.mongoDao.DiscordChannelMongoDao;
import com.discord.bot.mongoDao.WebSiteMongoDao;
import com.discord.bot.model.SubscribedChannel;
import com.discord.bot.model.WebSite;

import com.discord.bot.utils.HttpUtils;
import com.discord.bot.utils.IsStringUtil;
import com.discord.bot.utils.TimeUtils;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Builder
@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class DiscordService {

    IsStringUtil isStringUtil = new IsStringUtil();
    DiscordBot discordBot;
    DiscordChannelMongoDao discordChannelMongoDao;
    MongoOperations mongoOperation;
    WebSiteMongoDao websiteMongoDao;
    TimeUtils timeUtils;
    HttpUtils httpUtils;
    HelperToMarkDown helperToMarkDown;
    List<String> commandsList = Stream.of(Commands.values())
            .map(Commands::getCommand)
            .collect(Collectors.toList());


    @Autowired
    public DiscordService(@Lazy DiscordBot discordBot, DiscordChannelMongoDao discordChannelMongoDao, MongoOperations mongoOperation, WebSiteMongoDao websiteMongoDao, TimeUtils timeUtils, HttpUtils httpUtils, HelperToMarkDown helperToMarkDown) {
        this.discordBot = discordBot;
        this.discordChannelMongoDao = discordChannelMongoDao;
        this.mongoOperation = mongoOperation;
        this.websiteMongoDao = websiteMongoDao;
        this.timeUtils = timeUtils;
        this.httpUtils = httpUtils;
        this.helperToMarkDown = helperToMarkDown;
    }


    public Optional<SubscribedChannel> findChannelId(String channelId) {
        return discordChannelMongoDao.findSubscribedChannelByChannelId(channelId);
    }


    public void deleteWebsiteByChannelIdAndSiteFromDb(String siteToDelete, String channelId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(siteToDelete)
                .andOperator(Criteria.where("channelId").is(channelId)));
        val webSiteToDelete = mongoOperation.findOne(query, WebSite.class);
        assert webSiteToDelete != null;
        websiteMongoDao.delete(webSiteToDelete);
    }


    public SubscribedChannel registerChannel(SubscribedChannel newChannel) {
        log.warn("Channel Registered");
        return discordChannelMongoDao.save(newChannel);
    }


    public boolean isMsgCommand(String msg) {
        val splitMsgToCommandAndParam = msg.split(" ");
        var commandInString = "";
        var paramInString = "";

        for (int i = 0; i < splitMsgToCommandAndParam.length; i++) {
            if (i == 0) {
                commandInString = splitMsgToCommandAndParam[i].toLowerCase();
            }
            if (i == 1) {
                paramInString = splitMsgToCommandAndParam[i];
            }
        }
        return !commandInString.equals(Commands.ADD.getCommand()) && commandsList.contains(commandInString) && paramInString.equals("") ||
                commandInString.equals(Commands.ADD.getCommand()) && isStringUtil.IsStringUrlUtil(paramInString) ||
                commandInString.equals(Commands.DELETE.getCommand()) && paramInString.equals("") ||
                commandInString.equals(Commands.DELETE.getCommand());
    }


    public SubscribedChannel updateDbWithChannelWithWebsite(String channelId, WebSite newSite) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(channelId));
        val foundChannelToAddSites = Optional.ofNullable(mongoOperation.findOne(query, SubscribedChannel.class))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST));

        val existingSitesOfWebSites = foundChannelToAddSites.getTrackedWebsites();
        List<WebSite> newListOfWebsites = new ArrayList<>(existingSitesOfWebSites);
        newListOfWebsites.add(newSite);

        foundChannelToAddSites.setTrackedWebsites(newListOfWebsites);
        log.debug("Service -> updateChannelWithWebsite" + newListOfWebsites);
        return mongoOperation.save(foundChannelToAddSites);
    }

    public SubscribedChannel updateDbWithChannel(String channelId, SubscribedChannel updatedChannel) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(channelId));
        val foundChannel = Optional.ofNullable(mongoOperation.findOne(query, SubscribedChannel.class))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST));
        foundChannel.setTrackedWebsites(updatedChannel.getTrackedWebsites());
        log.debug("Service -> updateDbWithChannel" + updatedChannel);

        return mongoOperation.save(updatedChannel);
    }


    public WebSite addNewWebsitesToTrackToDb(String channelIdAsString, WebSite newSite, String currentTimeString) {
        try {
            List<PingAndDowntime> listOfPings = new ArrayList<>();
            val correctWebsiteString = httpUtils.getUrlAfterRedirect(newSite.getWebsiteUrl());
            val newPingDownTime = PingAndDowntime.builder()
                    .dateOfPingOrDowntime(currentTimeString)
                    .pingInMs(httpUtils.getWebsitePingInMs(correctWebsiteString))
                    .build();
            listOfPings.add(newPingDownTime);

            val newWebSite = WebSite.builder()
                    .uniqueId()
                    .websiteUrl(correctWebsiteString)
                    .channelId(channelIdAsString)
                    .dateOfCreation(currentTimeString)
                    .pingAndDowntimes(listOfPings)
                    .offlineAlarm(false)
                    .slowAlarm(false)
                    .build();
            return websiteMongoDao.save(newWebSite);
        } catch (IOException e) {
            e.printStackTrace();
            return WebSite.builder().build();
        }
    }


    public Optional<WebSite> isWebSiteWithChannelInDB(String id, String website) {
        return websiteMongoDao.findByChannelIdAndWebsiteUrl(id, website);
    }

    public String[] assignCommandAndParam(String[] messageSplitArray) {
        String[] commandAndParamArray = {"", ""};

        for (int i = 0; i < messageSplitArray.length; i++) {
            if (i == 0) {
                commandAndParamArray[0] = messageSplitArray[i];
            }
            if (i == 1) {
                commandAndParamArray[1] = messageSplitArray[i];
            } else {
                commandAndParamArray[1] = "";
            }
        }
        return commandAndParamArray;
    }


    public List<String> getAllWebsitesOfChannel(String channelId) {
        Optional<SubscribedChannel> subscribedChannelObject = findChannelId(channelId);
        return subscribedChannelObject.map(subscribedChannel ->
                subscribedChannel.getTrackedWebsites()
                        .stream()
                        .map(WebSite::getWebsiteUrl)
                        .collect(Collectors.toList()))
                .orElse(null);
    }

    public String getAllChannelWebsitesAsMarkDown(Map<Integer, String> listOfIdsAndWebsites) {
        //TODO: Improved MSG for user
        val guide = "\nWrite \"!delete NUMBER\" to remove a site from being tracked. ";
        val markdownStringList = new ArrayList<>(List.of("-> \n", guide, "```fix\n"));
        val setOfKeys = listOfIdsAndWebsites.keySet();
        setOfKeys.forEach(key -> {
            val keyAsString = key.toString();
            val site = listOfIdsAndWebsites.get(key);
            markdownStringList.add(helperToMarkDown.showSingleWebsitesWithKey(keyAsString, site));
        });
        markdownStringList.add("```");
        return markdownStringList.toString()
                .replaceAll(",", "")
                .replaceAll(",", "")
                .replaceAll("\\[", "")
                .replaceAll("]", "");
    }
}
