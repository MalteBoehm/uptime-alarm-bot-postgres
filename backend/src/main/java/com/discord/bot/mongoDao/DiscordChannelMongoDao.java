package com.discord.bot.mongoDao;

import com.discord.bot.model.SubscribedChannel;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface DiscordChannelMongoDao extends MongoRepository<SubscribedChannel, String> {
    Optional<SubscribedChannel> findSubscribedChannelByChannelId(String channelId);
}
