package com.discord.bot.mongoDao;

import com.discord.bot.model.WebSite;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface WebSiteMongoDao extends MongoRepository<WebSite, String> {
    Optional<WebSite> findByChannelIdAndWebsiteUrl(String id, String website);
}
