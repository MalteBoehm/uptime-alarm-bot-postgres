package com.discord.bot.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "webSites")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WebSite {

    // TODO : UUID über neues Feld einfügen
    @Id
    Long uniqueId;
    String websiteUrl;
    String channelId;
    List<PingAndDowntime> pingAndDowntimes;
    String dateOfCreation;
    boolean slowAlarm;
    boolean offlineAlarm;
}
