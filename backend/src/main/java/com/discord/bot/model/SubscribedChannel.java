package com.discord.bot.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "channel")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SubscribedChannel {
    // TODO : UUID über neues Feld einfügen
    @Id
    Long uniqueId;
    String channelId;
    String adminId;
    List<WebSite> trackedWebsites;
}
