package com.discord.bot.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;


@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PingAndDowntime {

    @Id
    Long uniqueId;
    String dateOfPingOrDowntime;
    int pingInMs;
}
