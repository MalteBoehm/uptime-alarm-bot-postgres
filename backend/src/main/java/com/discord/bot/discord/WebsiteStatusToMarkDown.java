package com.discord.bot.discord;


import com.discord.bot.enums.PingStatus;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class WebsiteStatusToMarkDown {

    public String onlineStatus(boolean onlineStatus) {
        if (!onlineStatus) {
            return """
                    ```diff
                    - Offline
                                            
                    ```
                    """;
        } else {
            return """
                    ```diff
                    + Online
                    ```
                    """;
        }
    }


    public String speedStatus(int currentStatus) {
        if (currentStatus == 0) {
            return """
                    ```diff
                    - Website is Offline or written wrong
                    ```
                    """;
        }
        if (currentStatus > 0 && currentStatus <= PingStatus.FAST.getMs()){
            return """
                    ```json
                    "The Website is very fast"
                    ```
                    """;
        }
        if (currentStatus > PingStatus.FAST.getMs() && currentStatus <= PingStatus.AVG.getMs()) {
            return """
                    ```fix
                    Website Could be faster
                    ```
                    """;
        }
        if (currentStatus > PingStatus.AVG.getMs() && currentStatus <= PingStatus.SLOW.getMs()) {
            return """
                    ```css
                    [Bad user experience - Very Slow Website]
                    ```
                    """;
        }
        if (currentStatus > PingStatus.SLOW.getMs() && currentStatus <= PingStatus.TOO_SLOW.getMs()) {
            return """
                    ```css
                    [Terrible user experience - You need to do something]
                    ```
                    """;
        }
        return "";
    }
}
