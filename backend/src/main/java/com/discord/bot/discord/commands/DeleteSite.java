package com.discord.bot.discord.commands;

import com.discord.bot.discord.DiscordBot;
import com.discord.bot.model.SubscribedChannel;
import com.discord.bot.model.WebSite;
import com.discord.bot.service.DiscordService;
import com.discord.bot.utils.IsStringUtil;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.channel.Channel;
import discord4j.core.object.entity.channel.MessageChannel;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.util.*;

@Slf4j
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class DeleteSite extends Command {

    public DeleteSite(DiscordBot bot, DiscordService service, Message msg, Channel channel, IsStringUtil isStringUtil) {
        super(bot, service, msg, channel, isStringUtil);
        this.needsRegisteredChannel = true;
    }

    @Override
    public void execute() {
        boolean canExecute = super.canExecute();
        if (!canExecute) return;

        val currentChannel = (MessageChannel) channel;
        val channel = msg != null ? msg.getChannel().block() : null;
        if (channel == null) return;

        val channelIdAsString = currentChannel.getId().asString();
        val messageSplitArray = msg.getContent().split(" ");

        val paramInArray = service.assignCommandAndParam(messageSplitArray);
        val paramInString = paramInArray[1];
        val listOfIdsAndWebsites = getWebsitesOfChannel(channelIdAsString);

        val noParam = "";
        // Option == 0 -> Returns msgs like write "!delete 1 to delete any site of your following sites: "Id 1: http://bild.de"
        if (paramInArray[1].equals(noParam)) {
            printWebsitesOfChannelToDiscordChat(listOfIdsAndWebsites);
        }
        // TODO Test
        else if (!paramInString.equals(noParam)) {
            botReplies.add(paramInString);
            Integer websiteKey = Integer.parseInt(paramInString);
            if (listOfIdsAndWebsites.containsKey(websiteKey)) {
                String siteToDelete = listOfIdsAndWebsites.get(websiteKey);
                deleteWebsiteOfChannelInDb(siteToDelete, channelIdAsString);
            }
        }
    }

    private Map<Integer, String> getWebsitesOfChannel(String channelId) {
        val allWebsitesOfChannel = service.getAllWebsitesOfChannel(channelId);
        val lengthOfWebsiteS = allWebsitesOfChannel.size();

        val webSiteMap = new HashMap<Integer, String>();
        for (int x = 0; x < lengthOfWebsiteS; x++) {
            webSiteMap.put(x + 1, allWebsitesOfChannel.get(x));
        }
        return webSiteMap;
    }

    private void printWebsitesOfChannelToDiscordChat(Map<Integer, String> listOfIdsAndWebsites) {
        val completeWebsitesMarkdown = service.getAllChannelWebsitesAsMarkDown(listOfIdsAndWebsites);
        botReplies.add(completeWebsitesMarkdown);
    }

    private void deleteWebsiteOfChannelInDb(String siteToDelete, String channelId) {
        val channel = service.findChannelId(channelId);
        if (channel.isPresent()) {
            val currentListOfWebsites = channel.get().getTrackedWebsites();
            val listWithoutDeletedSite = new ArrayList<WebSite>();
            currentListOfWebsites.forEach(site -> {
                if (!site.getWebsiteUrl().equals(siteToDelete)) {
                    listWithoutDeletedSite.add(site);
                }
            });
            val updatedChannel = SubscribedChannel.builder()
                    .channelId(channel.get().getChannelId())
                    .adminId(channel.get().getAdminId())
                    .trackedWebsites(listWithoutDeletedSite)
                    .build();
            channel.get().setTrackedWebsites(currentListOfWebsites);
            val returnedChannel= service.updateDbWithChannel(channelId, updatedChannel);
            assert(returnedChannel.equals(updatedChannel));

            deleteWebsiteTrackingOfDb(siteToDelete, channelId);
        }
    }

    private void deleteWebsiteTrackingOfDb(String siteToDelete, String channelId) {
        botReplies.add(channelId+ "ChannelID, site: " +siteToDelete);
        service.deleteWebsiteByChannelIdAndSiteFromDb(siteToDelete, channelId);
    }
}
