package com.discord.bot.discord;

import discord4j.core.DiscordClientBuilder;
import discord4j.core.GatewayDiscordClient;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
@FieldDefaults( level = AccessLevel.PRIVATE)
public class BotConfiguration {
    @Value("${token}")
    String token;


    @Bean
    public GatewayDiscordClient gatewayDiscordClient() {
        GatewayDiscordClient client = null;
        log.debug("GatewayDiscordClient");
        try {
            client = DiscordClientBuilder.create(token)
                    .build()
                    .login()
                    .block();
        }
        catch ( Exception exception ) {
            log.warn( "Token is wrong or not in env Variables!", exception );
        }
        return client;
    }
}
