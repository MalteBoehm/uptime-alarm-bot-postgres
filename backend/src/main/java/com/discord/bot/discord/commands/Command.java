package com.discord.bot.discord.commands;

import com.discord.bot.discord.DiscordBot;
import com.discord.bot.mongoDao.DiscordChannelMongoDao;
import com.discord.bot.utils.IsStringUtil;
import com.discord.bot.service.DiscordService;

import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.channel.Channel;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import java.util.LinkedList;
import java.util.List;


@FieldDefaults(level = AccessLevel.PROTECTED)
public abstract class Command {

    final DiscordService service;
    final DiscordBot bot;
    final Message msg;
    final Channel channel;
    final IsStringUtil isStringUtil;

    @Getter
    final List<String> botReplies;
    boolean needsRegisteredChannel;
    boolean needsUserTag;


    protected Command(DiscordBot bot, DiscordService service, Message msg, Channel channel, IsStringUtil isStringUtil) {
        this.bot = bot;
        this.service = service;
        this.msg = msg;
        this.channel = channel;
        this.botReplies = new LinkedList<>();
        this.needsRegisteredChannel = false;
        this.needsUserTag = false;
        this.isStringUtil = isStringUtil;
    }


    public abstract void execute();


    protected boolean canExecute() {
        boolean canExecute = true;
        if (this.needsRegisteredChannel) {
            if (service.findChannelId(channel.getId().asString()).isEmpty()) {
                canExecute = false;
                botReplies.add("Needs register");
            }
        }

        if (this.needsUserTag) {
            if (msg.getUserMentionIds().size() != 1) {
                canExecute = false;
                botReplies.add("Needs user tag");
            }
        }
        return canExecute;
    }
}
