package com.discord.bot.discord;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class HelperToMarkDown {

    public String showSingleWebsitesWithKey(String id, String website) {
        return """
                - {id}: {website}
                """.replace("{id}", id).replace("{website}", website);
    }
}
