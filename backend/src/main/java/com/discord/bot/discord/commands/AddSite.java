package com.discord.bot.discord.commands;

import com.discord.bot.discord.DiscordBot;
import com.discord.bot.discord.WebsiteStatusToMarkDown;
import com.discord.bot.utils.HttpUtils;
import com.discord.bot.utils.IsStringUtil;
import com.discord.bot.utils.TimeUtils;
import com.discord.bot.model.WebSite;
import com.discord.bot.service.DiscordService;

import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.channel.Channel;
import discord4j.core.object.entity.channel.MessageChannel;
import discord4j.rest.util.Color;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.io.IOException;
import java.time.Instant;


@Slf4j
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class AddSite extends Command {

    TimeUtils timeUtils = new TimeUtils();
    HttpUtils httpUtils = new HttpUtils();
    WebsiteStatusToMarkDown webStatus = new WebsiteStatusToMarkDown();


    public AddSite(DiscordBot bot, DiscordService service, Message msg, Channel channel, IsStringUtil isStringUtil) {
        super(bot, service, msg, channel, isStringUtil);
        this.needsRegisteredChannel = true;
    }


    @Override
    public void execute() {

        boolean canExecute = super.canExecute();
        if (!canExecute) return;

        val currentChannel = (MessageChannel) channel;
        val channel = msg != null ? msg.getChannel().block() : null;
        if (channel == null) return;

        val channelIdAsString = currentChannel.getId().asString();
        val currentTimeString = timeUtils.getCurrentTimeInDaysMonthYearsHoursMin();
        val messageSplitArray = msg.getContent().split(" ");

        val paramInArray = service.assignCommandAndParam(messageSplitArray);
        val paramInString = paramInArray[1];


        if (isStringUtil.IsStringUrlUtil(paramInString)) {
            addWebsitesToDb(paramInString, channelIdAsString, currentTimeString, currentChannel, channel);
        } else {
            channel.createMessage("\"" +paramInString+ "\"" + " is not a Website");
        }
    }


    private void addWebsitesToDb(String paramInString, String channelIdAsString, String currentTimeString, MessageChannel currentChannel, MessageChannel channel) {
        try {
            val websiteURL = httpUtils.getUrlAfterRedirect(paramInString);
            var isSiteOnline = httpUtils.isWebsiteOnline(websiteURL);
            var isSiteOffline = !httpUtils.isWebsiteOnline(websiteURL);
            var isWebsiteNotInDb = service.isWebSiteWithChannelInDB(channelIdAsString, websiteURL).isEmpty();
            var isWebsiteInDb = !isWebsiteNotInDb;
            WebSite newSite = WebSite.builder()
                    .websiteUrl(websiteURL)
                    .dateOfCreation(currentTimeString)
                    .offlineAlarm(false)
                    .slowAlarm(false)
                    .build();

            if (isWebsiteNotInDb && isSiteOnline) {
                int currentPing = httpUtils.getWebsitePingInMs(websiteURL);
                val savedWebsite = service.addNewWebsitesToTrackToDb(channelIdAsString, newSite, currentTimeString);
                assert(savedWebsite!=null);
                discordEmbedShowingSiteIsAdded(currentChannel, newSite, currentPing, currentTimeString);
                /* Updates MongoDB Collection channel */
                val updatedChannel = service.updateDbWithChannelWithWebsite(channelIdAsString, newSite);
                if(!updatedChannel.getTrackedWebsites().contains(newSite)){
                    return;
                }
            }
            if (isWebsiteInDb && isSiteOnline) {
                discordEmbedShowingSiteIsAlreadyInDb(channel, websiteURL);
                return;
            }
            if (isSiteOffline) {
                discordEmbedShowingSiteIsOffline(channel, paramInString);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void discordEmbedShowingSiteIsAdded(MessageChannel currentChannel, WebSite newSite, int currentPing, String currentTimeString) {
        currentChannel.createEmbed(spec ->
                spec.setColor(Color.GREEN)
                        .setAuthor("Your Site was added!", newSite.getWebsiteUrl(), "https://i.pinimg.com/originals/70/a5/52/70a552e8e955049c8587b2d7606cd6a6.gif")
                        .setThumbnail("https://i.pinimg.com/originals/70/a5/52/70a552e8e955049c8587b2d7606cd6a6.gif")
                        .setTitle(newSite.getWebsiteUrl())
                        .setUrl(newSite.getWebsiteUrl())
                        .setDescription("""
                                {online}
                                {ping}
                                """
                                .replace("{online}", webStatus.onlineStatus(true))
                                .replace("{ping}", webStatus.speedStatus(currentPing)))
                        .addField("Added", newSite.getWebsiteUrl(), true)
                        .addField("Time", currentTimeString, true)
                        .addField("TTFB/Ping", "" + currentPing, false)
                        .setFooter("Time:", "https://github.com/")
                        .setTimestamp(Instant.now())
        ).block();
    }


    private void discordEmbedShowingSiteIsAlreadyInDb(MessageChannel currentChannel, String paramInString) {
        currentChannel.createEmbed(spec ->
                spec.setColor(Color.YELLOW)
                        .setAuthor("Your Site is already added!", paramInString, "https://media4.giphy.com/media/USNlL9p2fxY6Q/giphy.gif")
                        .setImage("https://media4.giphy.com/media/USNlL9p2fxY6Q/giphy.gif")
                        .setTitle(paramInString)
                        .setDescription("""
                                You cannot add the site a second time.
                                """)
                        .setFooter("Time:", "https://github.com/")
                        .setTimestamp(Instant.now())
        ).block();
    }


    private void discordEmbedShowingSiteIsOffline(MessageChannel currentChannel, String paramInString) {
        currentChannel.createEmbed(spec ->
                spec.setColor(Color.RED)
                        .setAuthor("This Site does not exists!", paramInString, "https://upload.wikimedia.org/wikipedia/en/thumb/2/2c/Christian_cross_trans.svg/1200px-Christian_cross_trans.svg.png")
                        .setThumbnail("https://upload.wikimedia.org/wikipedia/en/thumb/2/2c/Christian_cross_trans.svg/1200px-Christian_cross_trans.svg.png")
                        .setImage("https://64.media.tumblr.com/e7cfe0074ae5fd4bd2f47735e9f53206/tumblr_mkwpqrtiJN1rsdpaso1_500.gif")
                        .setTitle(paramInString)
                        .setDescription("""
                                Check if you have written ${website} the wrong way by clicking on the link...
                                """
                                .replace("${website}", paramInString))
                        .setFooter("Time:", "https://github.com/")
        ).block();
    }
}
