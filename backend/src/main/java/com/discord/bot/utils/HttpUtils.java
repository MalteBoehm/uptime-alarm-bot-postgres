package com.discord.bot.utils;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

@Slf4j
@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class HttpUtils {
    public HttpUtils() {
    }


    public boolean isWebsiteOnline(String website) throws IOException {
        val urlStatusCode = getStatusCodeOfWebsite(website);
        val isWebsiteValid = urlStatusCode.equals("2xx") || urlStatusCode.equals("3xx") ? website : "";
        return !isWebsiteValid.equals("");
    }


    public int getWebsitePingInMs(String website) {
        try {
            var startTime = System.currentTimeMillis();
            Document doc = Jsoup.connect(website).get();
            doc.head();
            return (int) (System.currentTimeMillis() - startTime);
        } catch (Exception e) {
            log.debug("getWebsitePingInMs()  threw a Exception when it tried to connect to get() the content :" + e);
            return 0;
        }
    }


    public String getUrlAfterRedirect(String websiteString) throws IOException {
        val pingInMs = getWebsitePingInMs(websiteString);
        val newUrlResponseStatus = getStatusCodeOfWebsite(websiteString);
        val newUrl = newUrlResponseStatus.equals("2xx") || newUrlResponseStatus.equals("3xx") ? new URL(websiteString) : null;
        if (newUrl == null) {
            log.debug("Not a Website");
            return "";
        }

        val httpConnection = (HttpURLConnection) newUrl.openConnection();
        httpConnection.setInstanceFollowRedirects(false);
        httpConnection.connect();
        httpConnection.getInputStream();

        val isRedirect = httpConnection.getResponseCode() == HttpURLConnection.HTTP_MOVED_PERM || httpConnection.getResponseCode() == HttpURLConnection.HTTP_MOVED_TEMP;
        val isGoodUrl = httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK && !isRedirect;

        if (isGoodUrl && pingInMs > 0) {
            return websiteString;
        }
        if (isRedirect && pingInMs > 0) {
            return httpConnection.getHeaderField("Location");
        }
        return "";
    }

    protected String getStatusCodeOfWebsite(String website) throws WebClientResponseException {
        try {
            val websiteResponse = WebClient.builder()
                    .baseUrl(website)
                    .build()
                    .get()
                    .exchange().flatMap(response -> {
                                if(response.statusCode().is5xxServerError() || response.statusCode().value() >= 400) {
                                    response.body(((clientHttpResponse, context) -> clientHttpResponse.getStatusCode()));
                                }
                                return response.toBodilessEntity();
                            }
                    ).block(); //TODO change to retieve() because exchange() is deprecated

            assert websiteResponse != null;
            if (websiteResponse.getStatusCode().is4xxClientError()) {
                return "4xx";
            }
            if (websiteResponse.getStatusCode().is2xxSuccessful()) {
                return "2xx";
            }
            if (websiteResponse.getStatusCode().is3xxRedirection()) {
                return "3xx";
            }

        } catch (Exception e){
            log.warn("UnknownHostException: " + e);
            return "5xx";
        }
    return "";
    }
}
