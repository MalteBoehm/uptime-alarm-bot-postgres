package com.discord.bot.utils;

import org.junit.jupiter.api.DisplayName;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class IsStringUtilTest {
    // GIVEN
    IsStringUtil isStringUtil = new IsStringUtil();


    @ParameterizedTest
    @ValueSource(strings = {"https://www.tvnow.de/",
            "https://stackoverflow.com/questions/163360/regular-expression-to-match-urls-in-java", "https://www.golem.de/asdaasasdsadasdasdasdasdasss12dfsd"})
    @DisplayName(value = "Function Return true when it gets different sorts of URLS")
    void isStringUrlUtil_ReturnsTrue_WhenInputIsCorrect(String input) {
        // THEN
        assertTrue(isStringUtil.IsStringUrlUtil(input));
    }


    @DisplayName(value = "Function Return false when it gets different sorts of wrong URLS")
    @ParameterizedTest
    @ValueSource(strings = {"tvnow.de/",
            "//stackoverflow.com/questions/163360/regular-expression-to-match-urls-in-java",
            "ww.tvnow.de/", "tvnow.de","www.tvnow.de/", "www.tvnow.de"})
    void isStringUrlUtil_ReturnsFalse_WhenInputIsBad(String input) {
        // THEN
        assertFalse(isStringUtil.IsStringUrlUtil(input));
    }
}
