package com.discord.bot.utils;

import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.IOException;
import java.net.UnknownHostException;

import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

class HttpUtilsTest {

    HttpUtils httpUtils = new HttpUtils();
    @Test
    void isOnline_returns_isPresent_when_website_is_online() throws IOException {

        val actual  = httpUtils.isWebsiteOnline("https://stackoverflow.com/questions/50223891/how-to-extract-response-header-status-code-from-spring-5-webclient-clientrespo");
        assertTrue(actual);
    }

    @Test
    void isOnline_returns_isEmpty_when_website_is_offline() throws IOException {
        val actual  = httpUtils.isWebsiteOnline("https://stasdasdasdasdackoverflow.com/");
        assertFalse(actual);
    }

    @Test
    void getWebsitePingInMs() {
        // GIVEN
        val urlToPing = "https://www.golem.de/";
        // WHEN
        val actual = httpUtils.getWebsitePingInMs(urlToPing);
        // THEN
        assertTrue(actual > 0);
    }

    @Test
    void isWebsiteOnline_isOnline_return_true() throws IOException {
        val url = "https://www.google.com/";
        //WHEN
        val actual = httpUtils.isWebsiteOnline(url);
        //THEN
        assertTrue(actual);
    }

    @Test
    void isWebsiteOnline_isOffline_return_false() throws IOException {
        val url = "https://www.gasdasdoogle.com/";
        //WHEN
        val actual = httpUtils.isWebsiteOnline(url);
        //THEN
        assertFalse(actual);
    }


    @ParameterizedTest
    @CsvSource({"https://www.baeldung.com/parameterized-tests-junit-5/, https://www.baeldung.com/parameterized-tests-junit-5",
            "https://google.com/, https://www.google.com/",
            "https://bild.de, https://www.bild.de/", "https://welt.de/, https://www.welt.de/"})
    void getUrlAfterRedirect_getsIncorrectUrl_and_returns_correctVersion(String websitesInput, String expectedOutput) throws IOException {
         //WHEN
        val actual = httpUtils.getUrlAfterRedirect(websitesInput);
        //THEN
        assertThat(actual, is(expectedOutput));
    }


    @ParameterizedTest
    @CsvSource({"https://www.baeldung.com/parameterized-tests-junit-5, 2xx",
            "https://google.com/, 3xx",
            "https://www.bild.de/asd, 4xx",
            "https://www.wdelt.de/, 5xx"})
    void getStatusCodeOfWebsite_returns_all_status_codes(String websiteInput, String expectedStatusCode) throws UnknownHostException {
        //WHEN
        val actual = httpUtils.getStatusCodeOfWebsite(websiteInput);
        //THEN
        assertThat(actual, is(expectedStatusCode));
    }
}
